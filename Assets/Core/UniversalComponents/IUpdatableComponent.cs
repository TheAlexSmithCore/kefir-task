namespace Core.UniversalComponents{
    public interface IUpdatableComponent
    {
        public void Update();
    }
}
