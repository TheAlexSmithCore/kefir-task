namespace Core.UniversalComponents
{
    public class UniversalComponent : IComponent
    {
        private bool _isActivated = true;
        public bool IsActive
        {
            get => _isActivated;
            set
            {
                _isActivated = value;
                
                if(_isActivated){ OnActivated();}
                else {OnDeactivated();}
            }
        }

        public virtual void OnActivated()
        {
        }

        public virtual void OnDeactivated()
        {
        }
    }
}
