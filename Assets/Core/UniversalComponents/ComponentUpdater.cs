namespace Core.UniversalComponents
{
    public class ComponentUpdater : UniversalComponent, IUpdatableComponent
    {
        public ComponentUpdater(params IUpdatableComponent[] components)
        {
            _components = components;
        }

        private readonly IUpdatableComponent[] _components;

        public void Update()
        {
            if(!IsActive) {return; }

            foreach (IUpdatableComponent t in _components)
            {
                t.Update();
            }
        }
    }
}
