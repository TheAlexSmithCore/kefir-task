namespace Core.UniversalComponents
{
    public interface IComponent
    {
        public bool IsActive { get; set; }
        
        public void OnActivated();
        public void OnDeactivated();
    }
}
