namespace Core.Extendable
{
    public interface IResettableField
    {
        public void Reset();
    }
}