﻿namespace Core.Extendable
{
    public interface INormalizableField
    {
        public float Normalized();
    }
}