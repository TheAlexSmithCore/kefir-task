namespace Core.Extendable
{
    /// <summary>
    /// Abstract expandable value, allows you to use:
    /// InitValue, MinValue, MaxValue, CurrentValue, CurrentMaxValue
    /// </summary>
    /// <typeparam name="TValueType"></typeparam>
    public abstract class ExtendableField<TValueType> where TValueType : struct
    {
        protected ExtendableField(TValueType minValue, TValueType maxValue)
        {
            MinValue = minValue;
            MaxValue = maxValue;

            CurrentValue = MaxValue;
        }
        
        protected TValueType MinValue { private set; get; }
        protected TValueType MaxValue { private set; get; }
        private TValueType _currentValue;

        protected TValueType CurrentValue
        {
            get => _currentValue;
            set
            {
                _currentValue = value;
                CalculateBorders();
            }
        }

        public TValueType Get(FieldType type = FieldType.Current)
        {
            return GetValueOfType(type);
        }

        public void Set(TValueType value, FieldType type = FieldType.Current, SetType setType = SetType.Equate)
        {
            TValueType temp = GetValueOfType(type);
            SetValueOfType(CalculateValueChange(temp, value, type, setType), type);
        } 
        
        protected abstract TValueType CalculateValueChange(TValueType temp, TValueType value, FieldType type = default, SetType setType = default);
        protected abstract void CalculateBorders();
        
        private TValueType GetValueOfType(FieldType type = FieldType.Current)
        {
            switch (type)
            {
                case FieldType.Current:
                    return CurrentValue;
                case FieldType.MinValue:
                    return MinValue;
                case FieldType.MaxValue:
                    return MaxValue;
            }

            return CurrentValue;
        }
        
        private void SetValueOfType(TValueType value, FieldType type = FieldType.Current)
        {
            switch (type)
            {
                case FieldType.Current:
                    CurrentValue = value;
                    break;
                case FieldType.MinValue:
                    MinValue = value;
                    break;
                case FieldType.MaxValue:
                    MaxValue = value;
                    break;
            }
        }
    }
    
    public enum SetType : byte
    {
        Equate = 0,
        Sum = 1,
        Subtract = 2,
        Multiply = 3,
        Divide = 4
    }
    
    public enum FieldType : byte
    {
        Current = 0,
        MinValue = 1,
        MaxValue = 2
    }
}

