using Core.UniversalComponents;
using UnityEngine;

namespace Modules.Entities.Movables
{
    public interface IMovable : IUpdatableComponent
    {
        public Vector3 MoveVector { get; }
    }
}
