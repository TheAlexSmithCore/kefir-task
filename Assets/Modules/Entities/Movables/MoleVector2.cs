using Core.UniversalComponents;
using UnityEngine;

namespace Modules.Entities.Movables
{
    public struct MoleVector2 : IUpdatableComponent
    {
        public MoleVector2(Transform owner, Vector2 holeBounds)
        {
            _owner = owner;
            _ownerPosition = _owner.position;
            
            _holeBoundsMin = new Vector2(
                -holeBounds.x,
                -holeBounds.y
                );
            _holeBoundsMax = holeBounds;
        }

        public MoleVector2(Transform owner, Vector2 holeBoundsMin, Vector2 holeBoundsMax)
        {
            _owner = owner;
            _ownerPosition = _owner.position;
            
            _holeBoundsMin = new Vector2(
                holeBoundsMin.x,
                holeBoundsMin.y
            );
            _holeBoundsMax = new Vector2(
                holeBoundsMax.x,
                holeBoundsMax.y
            );
        }

        private Vector2 _ownerPosition;
        
        private readonly Transform _owner;

        private readonly Vector2 _holeBoundsMin;
        private readonly Vector2 _holeBoundsMax;

        public void Update()
        {
            _ownerPosition = _owner.position;
            
            if (_ownerPosition.x > _holeBoundsMax.x)  _ownerPosition.x = _holeBoundsMin.x;
            else if (_ownerPosition.x < _holeBoundsMin.x) _ownerPosition.x = _holeBoundsMax.x;
            else if (_ownerPosition.y > _holeBoundsMax.y) _ownerPosition.y = _holeBoundsMin.y;
            else if (_ownerPosition.y < _holeBoundsMin.y) _ownerPosition.y = _holeBoundsMax.y;

            _owner.position = _ownerPosition;
        }
    }
}
