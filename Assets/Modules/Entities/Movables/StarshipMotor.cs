using UnityEngine;

namespace Modules.Entities.Movables
{
    public class StarshipMotor : IMovable
    {
        public StarshipMotor(Rigidbody2D rigidbody)
        {
            _rigidbody = rigidbody;
        }
        
        private Rigidbody2D _rigidbody;

        private float moveSpeed = 400f;
        private float torqueSpeed = 315f;
        
        public void Update()
        {
            float vertical = LimitVerticalInput(Input.GetAxis("Vertical"));
            float horizontal = -Input.GetAxis("Horizontal");

            CalculateMovement(vertical);
            CalculateRotation(horizontal);
        }

        private void CalculateRotation(float horizontal)
        {
            _rigidbody.AddTorque((horizontal * torqueSpeed) * Time.deltaTime);
        }

        private void CalculateMovement(float vertical)
        {
            MoveVector = _rigidbody.transform.up;
            
            _rigidbody.AddForce(MoveVector * (vertical * moveSpeed * Time.deltaTime));
        }

        private float LimitVerticalInput(float value)
        {
            return value < 0 ? 0 : value;
        }

        public Vector3 MoveVector { get; private set; }
    }
}
