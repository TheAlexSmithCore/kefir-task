using UnityEngine;

namespace Modules.Entities.Movables
{
    public class ErraticMovement : IMovable
    {
        public ErraticMovement(Rigidbody2D rigidbody, int maxImpulse)
        {
            _rigidbody = rigidbody;

            float power = new System.Random().Next(maxImpulse / 3, maxImpulse);
            MoveVector = power * Random.insideUnitCircle;
            
            _rigidbody.AddTorque(power, ForceMode2D.Impulse);
        }

        private readonly Rigidbody2D _rigidbody;
        
        public void Update()
        {
            _rigidbody.velocity = MoveVector;
        }

        public Vector3 MoveVector { get; }
    }
}
